#include "mex.hpp"
#include "mexAdapter.hpp"

#include <vector>
using std::vector;
#include <assert.h>   /* assert */
#include <string>
#include <memory>
#include <algorithm>    // std::max

#include "Aligner.h"

using namespace matlab::mex;
using namespace matlab::data;
