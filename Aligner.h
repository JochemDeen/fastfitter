#include <vector>
#include <algorithm>

using std::vector;


class Aligner{

public:
Aligner(); //default constructor
Aligner(vector<vector<double>> scoremap, double holdpenalty, double skippenalty);
Aligner(vector<vector<double>> scoremap, double holdpenalty, double skippenalty, bool flipped);

void Run();
void align();

void initialize();
void FillMatrix();
void FindMaxscore();
void Traceback();

void FlipMatrix();

vector<vector<double>> fullScoreMatrix;
vector<vector<double>> scoreMatrix;
vector<vector<int>> fromi;
vector<vector<int>> fromj;

int size_1;
int size_2;

double holdpenalty;
double skippenalty;

bool flipped;

double maxscore;
int maxi;
int maxj;

std::vector<int> trace_j;
std::vector<int> trace_i;

};
