function trace = traceback(fromi, fromj, maxi,maxj)

endNotFound = true;
backi=maxi;
backj=maxj;
trace1=[];
trace2=[];
while endNotFound
    trace1 = [trace1; backi];
    trace2 = [trace2; backj];
    
    %disp([' backi : ' num2str(backi) ' backj : ' num2str(backj) ]);
    nexti = fromi(backi,backj);
    nextj = fromj(backi,backj);
    
    backi = nexti;
    backj = nextj;
    
    if backi == 0 || backj == 0
        endNotFound = false;
    end
end

trace = [flip(trace1),flip(trace2)];