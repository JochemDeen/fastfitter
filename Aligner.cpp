#include "Aligner.h"

Aligner::Aligner(){} //default constructor

Aligner::Aligner(vector<vector<double>> sm, double pen1, double pen2){
  scoreMatrix = sm;
  holdpenalty = pen1;
  skippenalty = pen2;

  size_1 = scoreMatrix.size();
  size_2 = scoreMatrix[0].size();

  flipped = false;
}

Aligner::Aligner(vector<vector<double>> sm, double pen1, double pen2, bool direction){
  scoreMatrix = sm;
  holdpenalty = pen1;
  skippenalty = pen2;

  size_1 = scoreMatrix.size();
  size_2 = scoreMatrix[0].size();

  flipped = direction;
}

void Aligner::initialize(){
  if (flipped){
    FlipMatrix();
  }

  for(int i=0; i<size_1; i++){
    vector<int> mminone;

    vector<double> z;
    for(int j=0; j<size_2; j++){
      mminone.push_back(-1);
      z.push_back(0);
    }
    fromi.push_back(mminone);
    fromj.push_back(mminone);

    fullScoreMatrix.push_back(z);
  }

  for(int i=0;i<size_1;i++){
    fullScoreMatrix[i][0] = scoreMatrix[i][0];
  }

  for(int j=0;j<size_2;j++){
    fullScoreMatrix[0][j] = scoreMatrix[0][j];
  }
}

void Aligner::FlipMatrix(){
  vector<vector<double>> flippedScoreMatrix;
  for(int i=size_1-1;i>=0;i--){
      flippedScoreMatrix.push_back(scoreMatrix[i]);
  }
  scoreMatrix = flippedScoreMatrix;
}

void Aligner::FillMatrix(){
  for(int i=1; i<size_1; i++){
    for(int j=1; j<size_2; j++){
      double thisscore = scoreMatrix[i][j];

      double score2;
      int step_i, step_j;
      double stepscore = fullScoreMatrix[i-1][j-1] + thisscore;
      double holdscore = fullScoreMatrix[i][j-1] + holdpenalty;
      double skipscore = fullScoreMatrix[i-1][j] + skippenalty;

      if (holdscore>skipscore){
        score2 = holdscore;
        step_i = i;
        step_j = j-1;
      }else{
        score2 = skipscore;
        step_i = i-1;
        step_j = j;
      }

      if (score2>stepscore){
        fullScoreMatrix[i][j] = score2;
        fromi[i][j] = step_i;
        fromj[i][j] = step_j;
      }else{
        fullScoreMatrix[i][j] = stepscore;
        fromi[i][j] = i-1;
        fromj[i][j] = j-1;
      }
    }
  }
};

void Aligner::Run(){
  initialize();

  FillMatrix();

  FindMaxscore();

  Traceback();
}

void Aligner::Traceback(){
  int backi,backj,nexti,nextj;
  backi = maxi;
  backj = maxj;

  bool EndNotFound = true;
  while (EndNotFound){
    if(flipped){
      trace_i.push_back(size_1 - backi);
    }else{
      trace_i.push_back(backi);
    }
    trace_j.push_back(backj);

    nexti = fromi[backi][backj];
    nextj = fromj[backi][backj];

    backi = nexti;
    backj = nextj;

    if (backi == -1 || backj == -1){
      EndNotFound = false;
    }

  }
  //invert vectors
  std::reverse(trace_i.begin(),trace_i.end());
  std::reverse(trace_j.begin(),trace_j.end());

}

void Aligner::FindMaxscore(){
  maxscore = fullScoreMatrix[0][size_2-1];
  for(int i=0;i<size_1;i++){
    if (fullScoreMatrix[i][size_2-1]>maxscore){
      maxscore = fullScoreMatrix[i][size_2-1];
      maxi=i;
      maxj=size_2-1;
    }
  }

  for(int j=0;j<size_2;j++){
    if (fullScoreMatrix[size_1-1][j]>maxscore){
      maxscore = fullScoreMatrix[size_1-1][j];
      maxi=size_1-1;
      maxj=j;
    }
  }

}
