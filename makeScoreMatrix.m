function scorematrix = makeScoreMatrix(trace1,trace2)
scorematrix = (ones(size(trace1(:)))*trace2(:)').*(trace1(:)*ones(size(trace2(:)')));


