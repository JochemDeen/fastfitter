function [maxi,maxj] = findmaxij(SM)
[~, idx ] = max([ max(SM(:,end)),max(SM(end,:))]);

switch idx
    case 1
        maxj = size(SM,2);
    [~, maxi] =  max(SM(:,end));
    case 2
        maxi = size(SM,1);
     [~, maxj] =  max(SM(end,:)); 
end
