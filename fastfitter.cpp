#include "fastfitter.h"

class MexFunction : public Function {
private:
  std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr;
  matlab::data::ArrayFactory factory;
public:
  /* Constructor for the class. */
  MexFunction()
  {
    matlabPtr = getEngine();
  }

  /* Helper function to generate an error message from given string,
   * and display it over MATLAB command prompt.
   */
  void displayError(std::string errorMessage)
  {
    ArrayFactory factory;
    matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
            0, std::vector<Array>({
      factory.createScalar(errorMessage) }));
  }

  /* Helper function to print output string on MATLAB command prompt. */
  void displayOnMATLAB(std::ostringstream stream)
  {
    ArrayFactory factory;
    matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("fprintf"),0, std::vector<Array>
            ({ factory.createScalar(stream.str())}));
  }

  void
  GetMatlabElements(matlab::data::Array matlabArray, vector<double> &Map){

    size_t total_num_of_elements = matlabArray.getNumberOfElements();

      for(int i=0;i<total_num_of_elements;i++){
            Map.push_back(matlabArray[i]);
      }
  }

  void
  GetMatlabElements(matlab::data::Array matlabArray, vector<vector<double>> &scoringMatrix){
    //size_t total_num_of_elements = matlabArray.getNumberOfElements();

      for(int i=0;i<matlabArray.getDimensions()[0];i++){
        vector<double> scoringRow;
        for(int j=0;j<matlabArray.getDimensions()[1];j++){
            scoringRow.push_back(matlabArray[i][j]);
          }
          scoringMatrix.push_back(scoringRow);
      }
  }

  void CheckInput(matlab::data::Array matlabArray,size_t size1, size_t size2){

    if (matlabArray.getDimensions().size() != 2 &&
    matlabArray.getDimensions()[0] !=size1 &&
    matlabArray.getDimensions()[1] !=size2){
      displayError("Incorrect size for scoring matrix, scoring matrix needs to be a 2D matrix with sizes of map1 and map2.");
    }
  }

  /* This is the gateway routine for the MEX-file. */
  void
  operator()(ArgumentList outputs, ArgumentList inputs) {
    //Checks
    if (inputs.size() < 1) {
      displayError("At least one input arguments is required:\n"
      "fastiffer(scoringMatrix)\n"
      "fastiffer(scoringMatrix,penalty)\n"
      "fastiffer(scoringMatrix,[holdpenalty,skippenalty])");
    }

    //load scoring matrix
    vector<vector<double>> scoringMatrix;
    matlab::data::Array const matlabArray3 = inputs[0];
    //CheckInput(matlabArray3,map1.size(),map2_size());
    GetMatlabElements(matlabArray3,scoringMatrix);

    //Load penalties
    double holdpenalty;
    double skippenalty;
    double res=70;
    if (inputs.size() > 1){
      matlab::data::Array const matlabArray4 = inputs[1];
      if (matlabArray4.getNumberOfElements() == 1){
        holdpenalty = matlabArray4[0];
        skippenalty = matlabArray4[0];
      }else{
        if (matlabArray4.getDimensions().size() > 2 || matlabArray4.getNumberOfElements()>2) {
          displayError("Incorrect input, penalties needs to be a 1 value or a 2-value matrix.");
        }
        holdpenalty = matlabArray4[0];
        skippenalty = matlabArray4[1];
      }
    }else{
      double holdPercentage = 15;
      holdpenalty = log(holdPercentage/100)-log(res/100);
      skippenalty = log(holdPercentage/100)-log(res/100);
    }

    Aligner bestAlignment;

    Aligner alignment1(scoringMatrix, holdpenalty, skippenalty);
    alignment1.Run();

    //Assume requesting fromi and fromj matrices means
    // only 1 direction will be alligned.
    if (outputs.size()>4){
      bestAlignment = alignment1;
    }else{
      Aligner alignment2(scoringMatrix, holdpenalty, skippenalty, true);
      alignment2.Run();

      if (alignment1.maxscore>alignment2.maxscore){
        bestAlignment = alignment1;
      }else{
        bestAlignment = alignment2;
      }
    }

    //Output 0 score
    outputs[0] = factory.createScalar(bestAlignment.maxscore);

    //output 1 Backtrace
      matlab::data::Array outArray =
              factory.createArray<int>({bestAlignment.trace_i.size(),2});
    for(int i=0;i<bestAlignment.trace_i.size();i++){
      outArray[i][0] = bestAlignment.trace_i[i]+1;
      outArray[i][1] = bestAlignment.trace_j[i]+1;
    }
    if (outputs.size()>1){
     outputs[1] = outArray;
     }

    //output 2 alignmentmatrix
      matlab::data::Array outArray2 =
              factory.createArray<double>({bestAlignment.fullScoreMatrix.size(),bestAlignment.fullScoreMatrix[0].size()});
      for(int i=0;i<bestAlignment.fullScoreMatrix.size();i++){
        for(int j=0;j<bestAlignment.fullScoreMatrix[i].size();j++){
          outArray2[i][j] = bestAlignment.fullScoreMatrix[i][j];
        }
      }
      if (outputs.size()>2){
       outputs[2] = outArray2;
       }

       //output 2 alignmentmatrix
       if (outputs.size()>4){
         matlab::data::Array outArray3 =
                 factory.createArray<int>({bestAlignment.fromi.size(),bestAlignment.fromi[0].size()});
         matlab::data::Array outArray4 =
                 factory.createArray<int>({bestAlignment.fromj.size(),bestAlignment.fromj[0].size()});
         for(int i=0;i<bestAlignment.fromi.size();i++){
           for(int j=0;j<bestAlignment.fromi[i].size();j++){
             outArray3[i][j] = bestAlignment.fromi[i][j] + 1;
             outArray4[i][j] = bestAlignment.fromj[i][j] + 1 ;
           }
          }
          outputs[3] = outArray3;
          outputs[4] = outArray4;
        }

  } //function
}; //class
