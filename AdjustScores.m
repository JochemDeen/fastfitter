function [trace, newscore] = AdjustScores(SM,minoverlap, fromi, fromj)

%Normalize Scoring Matrix by Size
SM_ = normalizeSMByLength(SM,minoverlap);

[maxi,maxj] = findmaxij(SM_);
newscore = SM(maxi,maxj);

trace = traceback(fromi, fromj, maxi,maxj);
