function SM = normalizeSMByLength(SM,minoverlap)
col=ones(size(SM,1),1).*min(size(SM));
col(1:min(minoverlap,max(col))) = minoverlap;
col(minoverlap:min(min(size(SM)), numel(col)))=minoverlap:min(min(size(SM)), numel(col));

row=ones(1, size(SM,2)-1).*min(size(SM));
row(1:min(minoverlap,max(row))) = minoverlap;
row(minoverlap:min(max(row), numel(row)))=minoverlap:min(max(row), numel(row));

SM(:,end) = SM(:,end)./col;

SM(end,1:end-1) = SM(end,1:end-1)./row;
